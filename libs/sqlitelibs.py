def create_new_db(dbpath,template=None,memory=False):
    #create sqlite database, either from scratch or as a copy of an existing "template database"
    #overwrites existing db!!!
    import os, shutil
    import sqlite3
    if memory==True:
        dbpath=':memory:'
    else:
        if os.path.exists(dbpath): os.remove(dbpath)
        if template is not None: shutil.copyfile(template,dbpath)
    conn=sqlite3.connect(dbpath)
    conn.enable_load_extension(True) 
    conn.execute("SELECT load_extension('mod_spatialite')")
    if template is None and memory==False: 
        #init can be very, very slow on computer with rotating disk - turn of the safety features for a moment...
        make_numbers_table(conn)
        sqexec(conn, 'PRAGMA synchronous=OFF')
        sqexec(conn, 'PRAGMA LOCKING_MODE=EXCLUSIVE')
        sqexec(conn, 'PRAGMA JOURNAL_MODE=MEMORY')
        sqexec(conn, "SELECT InitSpatialMetaData()")
        #close and reopen, to avoid that we accidentially run everything in the unsafe modes
        conn.close()
        conn=sqlite3.connect(dbpath)
        conn.enable_load_extension(True) 
        conn.execute("SELECT load_extension('mod_spatialite')")
    elif template is None and memory==True: 
        sqexec(conn, "SELECT InitSpatialMetaData()")
    return conn

def connect_spatial_db(dbpath):
    #connect to existing spatialite db
    import sqlite3
    conn=sqlite3.connect(dbpath)
    conn.enable_load_extension(True) 
    conn.execute("SELECT load_extension('mod_spatialite')")
    return conn

def make_numbers_table(connection):
    sqexec(connection,'create table numbers(n integer not null, primary key(n asc))')
    statement="insert into numbers(n) select row_number() over (order by '') from (select 1 from (select 0 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) a, (select 0 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) b, (select 0 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) c, (select 0 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) d, (select 0 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) e, (select 0 union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) f) derived"
    sqexec(connection, statement)
    
def import_shp(conn,shp_path,outtable,srid):
    #import shapefile "shp_path" into new table with name "outtable" in db specified by handle "outtable"
    pshape=shp_path.replace(".shp","")
    sqlstring='CREATE VIRTUAL TABLE testshape USING VirtualShape("' + pshape + '", CP1252, '+str(srid)+')'
    sqexec(conn,sqlstring)
    tscheme=conn.execute("pragma table_info('testshape')").fetchall()
    geodef=conn.execute("SELECT srid,geometry_type FROM vector_layers WHERE table_name='testshape'").fetchall()
    statement="CREATE TABLE "+outtable+" (PK_UID INTEGER PRIMARY KEY AUTOINCREMENT"
    for val in tscheme:
        if val[1]=='Geometry' or val[1]=='PK_UID' or val[1]=='PKUID':
            continue
        else:
            statement=statement+','+val[1]+' '+val[2]
    statement=statement+')'
    sqexec(conn, statement)
    gtype=['POINT','LINESTRING','POLYGON','MULTIPOINT','MULTILINESTRING','MULTIPOLYGON','GEOMETRYCOLLECTION'][geodef[0][1]-1]
    #AddGeometryColumn function does not work in spatialite 4.3.0
    #(missing geometry table errors when trying to insert data into the table below)
    #use Spatialite version 4.4.0 or higher
    sqexec(conn,"SELECT AddGeometryColumn('"+outtable+"','Geometry',"+str(srid)+",'"+gtype+"',2)")
    sqexec(conn,"SELECT CreateSpatialIndex('"+outtable+"','Geometry')")
    statement="INSERT INTO "+outtable+"(Geometry"
    for val in tscheme:
        if not (val[1]=='Geometry' or val[1]=='PK_UID' or val[1]=='PKUID'):
            statement=statement+','+val[1]
    statement=statement+") SELECT Geometry"
    for val in tscheme:
        if not (val[1]=='Geometry' or val[1]=='PK_UID' or val[1]=='PKUID'):
            statement=statement+','+val[1]
    statement=statement+" FROM testshape"
    sqexec(conn,statement)
    #use dropgeotable for proper clean up
    sqexec(conn,"SELECT DropGeoTable('testshape')")
    return 0

    
def sqexec(connection, statement):
    #execute an sql statement on a database connection created by
    #conn=sqlite3.connect(dbpathw)
    #conn.enable_load_extension(True) 
    #conn.execute("SELECT load_extension('mod_spatialite')")
    connection.execute(statement)
    connection.commit()

def add_new_columns(connection,table,columns):
    #add new columns to an existing table
    for column in columns:
        statement="ALTER TABLE "+table+" ADD COLUMN "+column
        sqexec(connection, statement)

def dissolve_polygon3(connection,inlayer,outlayer,sistep=250):
    import math
    print("CHECK OUTPUT. FUNCTION SOMETIMES LOOSES SOME POLYGONS AT THE MOMENT")
    #increases speed:
    #reduces number of polygons first by checking only neighbouring polygons on some small spatial scale
    #requires some understanding of the spatial scale at which it initially makes sense to search for neighbouring polygons (default 250m)
    #subsequently, spatial index and touch function are used to dissolve all polygons
    #if geometry problems arise, use buffer as in original geometry function (very heavy):
    #CastToGeometryCollection(Buffer(GUnion(Buffer(Geometry, 0.01)),-0.01))
    sqexec(connection,'UPDATE '+inlayer+' SET Geometry=SanitizeGeometry(Geometry)') 
    sqexec(connection,"SELECT UpdateLayerStatistics('"+inlayer+"')")
    curs=connection.cursor()
    min_x=curs.execute("SELECT extent_min_x FROM vector_layers_statistics WHERE table_name='"+inlayer+"'").fetchall()[0][0]
    max_x=curs.execute("SELECT extent_max_x FROM vector_layers_statistics WHERE table_name='"+inlayer+"'").fetchall()[0][0]
    min_y=curs.execute("SELECT extent_min_y FROM vector_layers_statistics WHERE table_name='"+inlayer+"'").fetchall()[0][0]
    max_y=curs.execute("SELECT extent_max_y FROM vector_layers_statistics WHERE table_name='"+inlayer+"'").fetchall()[0][0]
    curs.close()
    del curs
    dy=long((max_y-min_y)*10.0)
    for siinterval in range(2):
        print(siinterval)
        if siinterval==0:
            factor=10**(math.ceil(math.log(float(dy)/sistep,10))+1)
            sistring='round((X(Centroid(Geometry))-'+str(min_x)+')/'+str(sistep)+')*'+str(factor)+'+round(('+str(max_y)+'-Y(Centroid(Geometry)))/'+str(sistep)+')'
            create_geo_table(connection, 'tmp_work_diss', 'POLYGON', 25832, ['SI INTEGER'])
            sqexec(connection, "INSERT INTO tmp_work_diss (Geometry,SI) SELECT Geometry, "+sistring+" FROM "+inlayer)
            create_geo_table(connection, 'tmp_work_diss2', 'MULTIPOLYGON', 25832, ['type CHARACTER(12)','SI INTEGER'])
            sqexec(connection, 'INSERT INTO tmp_work_diss2 (Geometry) SELECT CastToMultiPolygon(GUnion(Geometry)) FROM tmp_work_diss GROUP BY SI')
        else:
            sqexec(connection,"CREATE TABLE tmp_ids AS SELECT a.pk_uid AS pk_uid FROM tmp_work_diss3 AS a JOIN tmp_work_diss3 AS b ON (ST_Touches(a.geometry, b.geometry) = 1 AND b.ROWID IN (SELECT ROWID FROM SpatialIndex WHERE f_table_name='tmp_work_diss3' AND search_frame = a.geometry)) GROUP BY a.pk_uid")
            create_geo_table(connection, 'tmp_work_diss2', 'GEOMETRYCOLLECTION', 25832, [])
            sqexec(connection,"INSERT INTO tmp_work_diss2 (Geometry) SELECT CastToGeometryCollection(GUnion(b.geometry)) FROM tmp_ids AS t JOIN tmp_work_diss3 AS b ON (b.pk_uid = t.pk_uid)")
            sqexec(connection,'DROP TABLE tmp_ids')
            sqexec(connection,'SELECT DropGeoTable("tmp_work_diss3")')
        sqexec(connection,'SELECT DropGeoTable("tmp_work_diss")')
        create_geo_table(connection, 'tmp_work_diss3', 'POLYGON', 25832, [])
        sqexec(connection,'INSERT INTO tmp_work_diss3 (Geometry) SELECT SanitizeGeometry(ST_GeometryN(Geometry, numbers.n)) FROM tmp_work_diss2 JOIN numbers ON numbers.n <= ST_NumGeometries(Geometry)')
        sqexec(connection,'SELECT DropGeoTable("tmp_work_diss2")')
        sqexec(connection,'VACUUM')
    create_geo_table(connection, outlayer, 'POLYGON', 25832, [])
    sqexec(connection,'INSERT INTO '+outlayer+' (Geometry) SELECT Geometry FROM tmp_work_diss3')
    sqexec(connection,'SELECT DropGeoTable("tmp_work_diss3")')
    sqexec(connection,'VACUUM')

def dissolve_polygon2(connection,inlayer,outlayer,buffer=False):
    #much faster than the version below...
    sqexec(connection,'UPDATE '+inlayer+' SET Geometry=SanitizeGeometry(Geometry)')
    #https://groups.google.com/forum/#!topic/spatialite-users/8s56dMcPqhE
    sqexec(connection,"CREATE TABLE tmp_ids AS SELECT a.pk_uid AS pk_uid FROM "+inlayer+" AS a JOIN "+inlayer+" AS b ON ((ST_Touches(a.geometry, b.geometry) = 1 OR ST_Intersects(a.geometry, b.geometry) = 1) AND b.ROWID IN (SELECT ROWID FROM SpatialIndex WHERE f_table_name='"+inlayer+"' AND search_frame = a.geometry)) GROUP BY a.pk_uid")
    create_geo_table(connection, 'tmp_diss', 'GEOMETRYCOLLECTION', 25832, [])
    if buffer:
        sqexec(connection,"INSERT INTO tmp_diss (Geometry) SELECT CastToGeometryCollection(Buffer(GUnion(Buffer(b.geometry, 0.01)),-0.01)) FROM tmp_ids AS t JOIN "+inlayer+" AS b ON (b.pk_uid = t.pk_uid)")
    else:
        sqexec(connection,"INSERT INTO tmp_diss (Geometry) SELECT CastToGeometryCollection(GUnion(b.geometry)) FROM tmp_ids AS t JOIN "+inlayer+" AS b ON (b.pk_uid = t.pk_uid)")
    create_geo_table(connection, outlayer, 'POLYGON', 25832, [])
    statement="INSERT INTO "+outlayer+"(Geometry) SELECT ST_GeometryN(Geometry, numbers.n) FROM tmp_diss JOIN numbers ON numbers.n <= ST_NumGeometries(Geometry)"
    sqexec(connection,statement)
    sqexec(connection,"INSERT INTO "+outlayer+" SELECT NULL, geometry FROM "+inlayer+" WHERE pk_uid NOT IN (SELECT pk_uid FROM tmp_ids)")
    sqexec(connection,'SELECT DropGeoTable("tmp_diss")')
    sqexec(connection,'DROP TABLE tmp_ids')
    #Sandro uses ST_Touches and no buffer - worth trying, but should be less robust to geometry problems...
        
def dissolve_polygon(connection,inlayer,outlayer,fields,whereclause=None,multi=False):
    #function is currently specific for parcels, will be generalized later
    #assumes that a number table "numbers exists in the db"
    sqexec(connection,'UPDATE '+inlayer+' SET Geometry=SanitizeGeometry(Geometry)')
    create_geo_table(connection, 'tmp_diss', 'GEOMETRYCOLLECTION', 25832, fields)
    if whereclause is None:
        statement='INSERT INTO tmp_diss(Geometry) SELECT CastToGeometryCollection(Buffer(GUnion(Buffer(Geometry, 0.01)),-0.01)) FROM '+inlayer
    else:
        statement='INSERT INTO tmp_diss(Geometry) SELECT CastToGeometryCollection(Buffer(GUnion(Buffer(Geometry, 0.01)),-0.01)) FROM '+inlayer+' WHERE ' + whereclause
    sqexec(connection,statement)
    #sqexec(connection,"UPDATE tmp_diss SET GEOMETRY=CastToGeometryCollection(MakeValid(GEOMETRY))") #works if not sanitized before?
    if multi==False:
        create_geo_table(connection, outlayer, 'POLYGON', 25832, fields)
        statement="INSERT INTO "+outlayer+"(Geometry) SELECT ST_GeometryN(Geometry, numbers.n) FROM tmp_diss JOIN numbers ON numbers.n <= ST_NumGeometries(Geometry)"
    else:
        create_geo_table(connection, outlayer, 'MULTIPOLYGON', 25832, fields)
        statement="INSERT INTO "+outlayer+"(Geometry) SELECT CastToMultiPolygon(Geometry) FROM tmp_diss"
    sqexec(connection,statement)
    #sqexec(connection,'DELETE FROM '+outlayer+' WHERE Geometry IS NULL')
    sqexec(connection,"SELECT DropGeoTable('tmp_diss')")

def union_polygon_layer(connection,inlayer1,inlayer2,outlayer,pworkdb):
    #compute ArcGIS style union between inlayer1 and inlayer2
    #clean data before executing!. individual input layers must not have overlapping polygongs. use dissolve2 to remove these. otherwise GEOS gives self-intersection errors
    #Forum recipes - some of them do not work
    #https://gis.stackexchange.com/questions/83/separate-polygons-based-on-intersection-using-postgis
    #https://gis.stackexchange.com/questions/31310/acquiring-arcgis-like-speed-in-postgis/31562#31562
    #https://postgis.net/2014/03/14/tip_intersection_faster/
    #works, try to speed up using spatial indices
    print("individual input layers must not have overlapping polygongs. use dissolve2 to remove these. otherwise GEOS gives self-intersection errors")
    create_geo_table(connection, 'tmp_boundaries', 'MULTIPOLYGON', 25832,[])
    create_geo_table(connection, 'ilayer1', 'MULTIPOLYGON', 25832,[])
    create_geo_table(connection, 'ilayer2', 'MULTIPOLYGON', 25832,[])
    #keep only valid geometries
    sqexec(connection,'INSERT INTO ilayer1(Geometry) SELECT CastToMultiPolygon(ST_MakeValid(ST_Buffer(ST_Buffer(Geometry,-0.1),0.1))) FROM '+inlayer1)
    sqexec(connection,'INSERT INTO ilayer2(Geometry) SELECT CastToMultiPolygon(ST_MakeValid(ST_Buffer(ST_Buffer(Geometry,-0.1),0.1))) FROM '+inlayer2)
    sqexec(connection,'DELETE FROM ilayer1 WHERE Geometry IS NULL')
    sqexec(connection,'DELETE FROM ilayer2 WHERE Geometry IS NULL')
    ####Insert difference between ilayer1 and intersection area with ilayer2
    sqexec(connection,'INSERT INTO tmp_boundaries(Geometry) SELECT CastToMultiPolygon(ST_Difference(aa.Geometry,bb.Geometry)) FROM ilayer1 AS aa JOIN (SELECT a.PK_UID AS AUID, CastToMultiPolygon(ST_Collect(b.Geometry)) AS Geometry FROM ilayer1 AS a JOIN ilayer2 AS b ON ST_Intersects(a.Geometry,b.Geometry) GROUP BY a.PK_UID) AS bb ON aa.PK_UID=bb.AUID')
    ###Insert intersection area of ilayer1 and ilayer 2 - Speed version
    sqexec(connection,'INSERT INTO tmp_boundaries(Geometry) SELECT CASE WHEN ST_CoveredBy(a.Geometry,b.Geometry) THEN a.Geometry ELSE CastToMultiPolygon(ST_Intersection(a.Geometry,b.Geometry)) END FROM ilayer1 AS a, ilayer2 as b WHERE (ST_Intersects(a.Geometry,b.Geometry) AND a.ROWID IN (SELECT ROWID FROM SpatialIndex WHERE f_table_name="ilayer1" AND search_frame=b.Geometry))')
    ###insert features from ilayer1 that do not overlap with ilayer 2
    sqexec(connection,'INSERT INTO tmp_boundaries(Geometry) SELECT a.Geometry FROM ilayer1 AS a LEFT JOIN (SELECT aa.PK_UID AS auid, max(bb.PK_UID) AS buid FROM ilayer1 AS aa, ilayer2 AS bb WHERE ((ST_Intersects(aa.Geometry,bb.Geometry) OR ST_Touches(aa.Geometry,bb.Geometry)) AND (aa.ROWID IN (SELECT ROWID FROM SpatialIndex WHERE f_table_name="ilayer1" AND search_frame=bb.Geometry))) GROUP BY aa.PK_UID) AS b ON a.PK_UID=b.auid WHERE b.buid IS NULL')
    #explode multipolygons and save
    create_geo_table(connection, 'tmp_boundaries2', 'POLYGON', 25832,[])
    sqexec(connection,'INSERT INTO tmp_boundaries2(Geometry) SELECT CastToPolygon(ST_GeometryN(Geometry, numbers.n)) FROM tmp_boundaries JOIN numbers ON numbers.n <= ST_NumGeometries(Geometry)')
    import pdb
    pdb.set_trace()
    #copy_geo_table_external(connection,'tmp_boundaries2','tmp_out',pworkdb,'diskdb',withdata=True,dropold=False)
    
def intersect_polygon(connection,inlayer1, inlayer2,outlayer,fields,multi=False):
    #function is currently specific for parcels, will be generalized later
    #assumes that a number table "numbers exists in the db"
    create_geo_table(connection, 'tmp_inter', 'GEOMETRYCOLLECTION', 25832, fields)
    statement='INSERT INTO tmp_inter(Geometry) SELECT CastToGeometryCollection(Intersection('+inlayer1+'.Geometry,'+inlayer2+'.Geometry)) FROM '+inlayer1+','+inlayer2+' WHERE '+inlayer1+'.ROWID IN (SELECT ROWID FROM SpatialIndex WHERE f_table_name="'+inlayer1+'" AND search_frame='+inlayer2+'.Geometry)'
    sqexec(connection,statement)
    sqexec(connection,"UPDATE tmp_inter SET Geometry=SanitizeGeometry(Geometry)")
    sqexec(connection,"DELETE FROM tmp_inter WHERE Geometry IS NULL")
    if multi==False:
        create_geo_table(connection, outlayer, 'POLYGON', 25832, fields)
        statement="INSERT INTO "+outlayer+"(Geometry) SELECT ST_GeometryN(Geometry, numbers.n) FROM tmp_inter JOIN numbers ON numbers.n <= ST_NumGeometries(Geometry)"
    else:
        create_geo_table(connection, outlayer, 'MULTIPOLYGON', 25832, fields)
        statement="INSERT INTO "+outlayer+"(Geometry) SELECT CastToMultiPolygon(Geometry) FROM tmp_inter"
    sqexec(connection,statement)
    sqexec(connection,"SELECT DropGeoTable('tmp_inter')")

def merge_geo_tables(connection, inlayer1, inlayer2, outlayer):
    #merge geometries and data in two tables
    #rename attribute fields with equal names
    gd1=connection.execute("SELECT srid,geometry_type FROM geometry_columns WHERE f_table_name='"+inlayer1.lower()+"'").fetchall()
    gd2=connection.execute("SELECT srid,geometry_type FROM geometry_columns WHERE f_table_name='"+inlayer2.lower()+"'").fetchall()
    if gd1[0][1]!=gd2[0][1]:
        print("trying to merge layers with different geometry types")
        raise
    if gd1[0][0]!=gd2[0][0]:
        print("merge layers have different SRID")
    #######################
    #create table with all attributes
    tscheme=connection.execute("pragma table_info ('"+inlayer1+"')").fetchall()
    fields1=[x for x in tscheme]
    fields1_out=[x if not x[1]=='PK_UID' else (x[0], x[1]+'_1',x[2]) for x in fields1]
    tscheme=connection.execute("pragma table_info ('"+inlayer2+"')").fetchall()
    fields2=[x for x in tscheme]
    fields2_out=[xx if (not (xx[1] in [x[1] for x in fields1] or xx[1]=='PK_UID')) or xx[1].lower()=='geometry' else (xx[0], xx[1]+'_2',xx[2]) for xx in fields2]
    statement="CREATE TABLE "+outlayer+" (PK_UID INTEGER PRIMARY KEY AUTOINCREMENT,"
    statement=statement+','.join([x[1]+' '+x[2] for x in fields1_out if not x[1].lower()=='geometry'])
    statement=statement+','+','.join([x[1]+' '+x[2] for x in fields2_out if not x[1].lower()=='geometry'])
    statement=statement+')'
    sqexec(connection, statement)
    #######################
    #add geometry column
    gtype=['POINT','LINESTRING','POLYGON','MULTIPOINT','MULTILINESTRING','MULTIPOLYGON','GEOMETRYCOLLECTION'][gd1[0][1]-1]
    srid=gd1[0][0]
    sqexec(connection,"SELECT AddGeometryColumn('"+outlayer+"','Geometry',"+str(srid)+",'"+gtype+"',2)")
    sqexec(connection,"SELECT CreateSpatialIndex('"+outlayer+"','Geometry')")
    for i in range(2):
        fields = [fields1,fields2][i]
        fields_out = [fields1_out,fields2_out][i]
        gname=[x[1] for x in fields if x[1].lower()=='geometry']
        fieldnames_in=','.join(gname+[x[1] for x in fields if x[1].lower()!='geometry'])
        fieldnames_out=','.join(['Geometry']+[x[1] for x in fields_out if x[1].lower()!='geometry'])
        sqexec(connection,"INSERT INTO "+outlayer+"("+fieldnames_out+") SELECT " +fieldnames_in+" FROM "+[inlayer1,inlayer2][i])

def difference_polygon(connection,inlayer1, inlayer2,outlayer,fields):
    #function is currently specific for parcels, will be generalized later
    #assumes that a number table "numbers exists in the db"
    create_geo_table(connection, 'tmp_diff', 'GEOMETRYCOLLECTION', 25832, fields)
    statement='INSERT INTO tmp_diff(Geometry) SELECT CastToGeometryCollection(Difference('+inlayer1+'.Geometry,'+inlayer2+'.Geometry)) FROM '+inlayer1+','+inlayer2+' WHERE '+inlayer1+'.ROWID IN (SELECT ROWID FROM SpatialIndex WHERE f_table_name="'+inlayer1+'" AND search_frame='+inlayer2+'.Geometry)'
    sqexec(connection,statement)
    sqexec(connection,"UPDATE tmp_diff SET Geometry=SanitizeGeometry(Geometry)")
    sqexec(connection,"DELETE FROM tmp_diff WHERE Geometry IS NULL")
    create_geo_table(connection, outlayer, 'POLYGON', 25832, fields)
    statement="INSERT INTO "+outlayer+"(Geometry) SELECT ST_GeometryN(Geometry, numbers.n) FROM tmp_diff JOIN numbers ON numbers.n <= ST_NumGeometries(Geometry)"
    sqexec(connection,statement)
    sqexec(connection,"SELECT DropGeoTable('tmp_diff')")  
    
def get_table_fields(connection, name, exclude=[]):
    #get list of all fields in a table - useful for creating insert statements where we want to copy a subset of data to a new table
    tscheme=connection.execute("pragma table_info('"+name+"')").fetchall()
    fields=[]
    for val in tscheme:
        if val[1]=='PK_UID' or val[1] in exclude:
            continue
        else:
            fields.append(val[1])
    return fields
    
def copy_geo_table(connection, name, newname,sourcedbalias=None,withdata=True,dropold=False,exclude=[],cliplayer=None):
    #copy a table with geometry
    #EXAMPLE: copy_geo_table(conn, 'buildings_tmp', 'buildings_new','TMP',withdata=True,dropold=True)
    if not sourcedbalias==None:
        tscheme=connection.execute("pragma "+sourcedbalias+".table_info ('"+name+"')").fetchall()
    else:
        tscheme=connection.execute("pragma table_info ('"+name+"')").fetchall()
    statement="CREATE TABLE "+newname+" (PK_UID INTEGER PRIMARY KEY AUTOINCREMENT"
    fields=[]
    for val in tscheme:
        if val[1]=='PK_UID' or val[1] in exclude:
            continue
        elif val[1].lower()=='geometry':
            fields.append(val[1])
        else:
            statement=statement+','+val[1]+' '+val[2]
            fields.append(val[1])
    statement=statement+')'
    sqexec(connection, statement)
    if not sourcedbalias==None:
        geodef=connection.execute("SELECT srid,geometry_type FROM "+sourcedbalias+".geometry_columns WHERE f_table_name='"+name.lower()+"'").fetchall()
    else:
        geodef=connection.execute("SELECT srid,geometry_type FROM geometry_columns WHERE f_table_name='"+name.lower()+"'").fetchall()
    srid=geodef[0][0]
    gtype=['POINT','LINESTRING','POLYGON','MULTIPOINT','MULTILINESTRING','MULTIPOLYGON','GEOMETRYCOLLECTION'][geodef[0][1]-1]
    sqexec(connection,"SELECT AddGeometryColumn('"+newname+"','Geometry',"+str(srid)+",'"+gtype+"',2)")
    sqexec(connection,"SELECT CreateSpatialIndex('"+newname+"','Geometry')")
    fields_standard=[x.replace('GEOMETRY','Geometry') for x in fields] #enforce standard naming of geometry
    if withdata:
        if cliplayer is None:
            #copy the data
            if not sourcedbalias==None:
                sqexec(connection,"INSERT INTO "+newname+"("+','.join(fields_standard)+") SELECT "+','.join(fields)+" FROM "+sourcedbalias+'.'+name)
            else:
                sqexec(connection,"INSERT INTO "+newname+"("+','.join(fields_standard)+") SELECT "+','.join(fields)+" FROM "+name)
        else:
            #import the data from external DB. then find those that intersect our extent polygon and save only those together with intersection geometry
            fields_ng=[x for x in fields_standard if not x=='Geometry']
            create_geo_table(connection, 'tmp_is_copy2', 'GEOMETRYCOLLECTION', srid, fields_ng)
            if not sourcedbalias==None:
                copy_geo_table(connection, name, 'tmp_is_copy',sourcedbalias=sourcedbalias)
                statement='INSERT INTO tmp_is_copy2(Geometry,'+','.join(fields_ng)+') SELECT CastToGeometryCollection(Intersection(tmp_is_copy.Geometry,'+cliplayer+'.Geometry)),'+','.join(fields_ng)+' FROM tmp_is_copy JOIN '+cliplayer+' ON tmp_is_copy.ROWID IN (SELECT ROWID FROM SpatialIndex WHERE f_table_name="tmp_is_copy" AND search_frame='+cliplayer+'.geometry) AND (ST_Intersects(tmp_is_copy.geometry, '+cliplayer+'.geometry) = 1)'
                sqexec(connection,statement)
                sqexec(connection,"SELECT DropGeoTable('tmp_is_copy')")               
                #t0=time.clock();test2=cursor.execute('SELECT tmp_is_copy.dyn_id FROM tmp_is_copy JOIN '+cliplayer+' ON tmp_is_copy.ROWID IN (SELECT ROWID FROM SpatialIndex WHERE f_table_name="tmp_is_copy" AND search_frame='+cliplayer+'.geometry) AND (ST_Intersects(tmp_is_copy.geometry, '+cliplayer+'.geometry) = 1)').fetchall();t1=time.clock()
                ##0.0640618s
                #             
                #t0=time.clock();test2=cursor.execute('SELECT tmp_is_copy.dyn_id FROM tmp_is_copy, clip WHERE ST_Intersects(tmp_is_copy.Geometry,clip.Geometry)').fetchall();t1=time.clock()
                ##0.1638282999999987s
            else:
                statement='INSERT INTO tmp_is_copy2(Geometry,'+','.join(fields_ng)+') SELECT CastToGeometryCollection(Intersection('+name+'.Geometry,'+cliplayer+'.Geometry)),'+','.join(fields_ng)+' FROM '+name+' JOIN '+cliplayer+' ON '+name+'.ROWID IN (SELECT ROWID FROM SpatialIndex WHERE f_table_name="'+name+'" AND search_frame='+cliplayer+'.geometry) AND (ST_Intersects('+name+'.geometry, '+cliplayer+'.geometry) = 1)'
                sqexec(connection,statement)
            caststatement=['CastToPoint','CastToLinestring','CastToPolygon','CastToMultiPoint','CastToMultiLinestring','CastToMultiPolygon','CastToGeometryCollection'][geodef[0][1]-1]
            statement="INSERT INTO "+newname+"(Geometry,"+",".join(fields_ng)+") SELECT "+caststatement+"(ST_GeometryN(Geometry, numbers.n)),"+",".join(fields_ng)+" FROM tmp_is_copy2 JOIN numbers ON numbers.n <= ST_NumGeometries(Geometry)"
            sqexec(connection,statement)
            sqexec(connection,"SELECT DropGeoTable('tmp_is_copy2')")
    if dropold:
        if not sourcedbalias==None:
            print("dropping in attached database not implemented")
        else:
            sqexec(connection,"SELECT DropGeoTable('"+name+"')")

    #create_geo_table(connection, 'tmp_diff', 'GEOMETRYCOLLECTION', 25832, fields)
    #statement='INSERT INTO tmp_diff(Geometry) SELECT CastToGeometryCollection(Difference('+inlayer1+'.Geometry,'+inlayer2+'.Geometry)) FROM '+inlayer1+','+inlayer2+' WHERE '+inlayer1+'.ROWID IN (SELECT ROWID FROM SpatialIndex WHERE f_table_name="'+inlayer1+'" AND search_frame='+inlayer2+'.Geometry)'
    #sqexec(connection,statement)
    #sqexec(connection,"UPDATE tmp_diff SET Geometry=SanitizeGeometry(Geometry)")
    #sqexec(connection,"DELETE FROM tmp_diff WHERE Geometry IS NULL")
    #create_geo_table(connection, outlayer, 'POLYGON', 25832, fields)
    #statement="INSERT INTO "+outlayer+"(Geometry) SELECT ST_GeometryN(Geometry, numbers.n) FROM tmp_diff JOIN numbers ON numbers.n <= ST_NumGeometries(Geometry)"
    #sqexec(connection,statement)
    #sqexec(connection,"SELECT DropGeoTable('tmp_diff')")    



def copy_table_external(connection,name,newname,outputdb,outputdbalias,withdata=True,dropold=False):
    #copy a table to an external database (e.g., from memory to a physical db)
    #this function "manually opens" the output db and creates a new table before copying
    #it is assumed that the output database is attached to the source connection under name "outputdbalias"
    #EXAMPLE: copy_table_external(conn2,'blist','blist',workdb,'diskdb',withdata=True,dropold=False)
    conn_out=connect_spatial_db(outputdb)
    tscheme=connection.execute("pragma table_info ('"+name+"')").fetchall()
    statement="CREATE TABLE "+newname+" (PK_UID INTEGER PRIMARY KEY AUTOINCREMENT"
    fields=[]
    for val in tscheme:
        if val[1]=='PK_UID':
            continue
        else:
            statement=statement+','+val[1]+' '+val[2]
            fields.append(val[1])
    statement=statement+')'
    sqexec(conn_out, statement)
    #
    conn_out=None
    if withdata:
        sqexec(connection,"INSERT INTO "+outputdbalias+'.'+newname+"("+','.join(fields)+") SELECT "+','.join(fields)+" FROM "+name)
    if dropold:
        sqexec(connection,"SELECT DROP TABLE "+name)

def copy_geo_table_external(connection,name,newname,outputdb,outputdbalias,withdata=True,dropold=False):
    #copy a table with geometry to an external database (e.g., from memory to a physical db)
    #this function "manually opens" the output db and creates a new geometry table before copying
    #it is assumed that the output database is attached to the source connection under name "outputdbalias"
    #EXAMPLE: copy_geo_table_external(conn2,'areas_nodev_mem','areas_nodev_mem',workdb,'diskdb',withdata=True,dropold=False)
    #import pdb
    #pdb.set_trace()
    conn_out=connect_spatial_db(outputdb)
    tscheme=connection.execute("pragma table_info ('"+name+"')").fetchall()
    statement="CREATE TABLE "+newname+" (PK_UID INTEGER PRIMARY KEY AUTOINCREMENT"
    fields=[]
    for val in tscheme:
        if val[1]=='PK_UID':
            continue
        elif val[1].lower()=='geometry':
            fields.append(val[1])
        else:
            statement=statement+','+val[1]+' '+val[2]
            fields.append(val[1])
    statement=statement+')'
    sqexec(conn_out, statement)
    #
    geodef=connection.execute("SELECT srid,geometry_type FROM geometry_columns WHERE f_table_name='"+name.lower()+"'").fetchall()
    srid=geodef[0][0]
    gtype=['POINT','LINESTRING','POLYGON','MULTIPOINT','MULTILINESTRING','MULTIPOLYGON','GEOMETRYCOLLECTION'][geodef[0][1]-1]
    sqexec(conn_out,"SELECT AddGeometryColumn('"+newname+"','Geometry',"+str(srid)+",'"+gtype+"',2)")
    sqexec(conn_out,"SELECT CreateSpatialIndex('"+newname+"','Geometry')")
    conn_out=None
    fields_standard=[x.replace('GEOMETRY','Geometry') for x in fields] #enforce standard naming of geometry
    if withdata:
        sqexec(connection,"INSERT INTO "+outputdbalias+'.'+newname+"("+','.join(fields_standard)+") SELECT "+','.join(fields)+" FROM "+name)
    if dropold:
        sqexec(connection,"SELECT DropGeoTable('"+name+"')")
            
def copy_table(connection, name, newname,sourcedbalias=None,withdata=True,dropold=False):
    #copy a table without geometry
    #EXAMPLE: copy_table(conn, 'households1', 'households2','TMP',withdata=True,dropold=True)
    if not sourcedbalias==None:
        tscheme=connection.execute("pragma "+sourcedbalias+".table_info ('"+name+"')").fetchall()
    else:
        tscheme=connection.execute("pragma table_info ('"+name+"')").fetchall()
    statement="CREATE TABLE "+newname+" (PK_UID INTEGER PRIMARY KEY AUTOINCREMENT"
    fields=[]
    for val in tscheme:
        if val[1]=='PK_UID':
            continue
        else:
            statement=statement+','+val[1]+' '+val[2]
            fields.append(val[1])
    statement=statement+')'
    sqexec(connection, statement)
    if fields==[]: withdata=False
    if withdata:
        if not sourcedbalias==None:
            sqexec(connection,"INSERT INTO "+newname+"("+','.join(fields)+") SELECT "+','.join(fields)+" FROM "+sourcedbalias+'.'+name)
        else:
            sqexec(connection,"INSERT INTO "+newname+"("+','.join(fields)+") SELECT "+','.join(fields)+" FROM "+name)
    if dropold:
        if not sourcedbalias==None:
            print("dropping in attached database not implemented")
        else:
            sqexec(connection,"DROP Table "+name)

def copy_column_to_table(connection,outtable,srctable,linkf_out,linkf_src,dataf_out,dataf_src):
    #copy field dataf_src[0] from srctable into outtable under the name dataf_out[0]
    #the tables are linked through fields linkf_out and linkf_src
    #all existing records in outtable are preserved
    #EXAMPLE: copy_column_to_table(conn,'people_MU_catch','people_subcatch','CATCHID','CATCHID',['p'+str(year),'INTEGER'],['people','INTEGER'])
    copy_table(connection,outtable,'tmp',withdata=False)
    sqexec(connection,'ALTER TABLE tmp ADD '+str(dataf_out[0])+' '+dataf_out[1])
    sqexec(connection,'INSERT INTO tmp('+",".join(get_table_fields(connection, outtable))+','+str(dataf_out[0])+') SELECT t1.'+",t1.".join(get_table_fields(connection, outtable))+',t2.'+str(dataf_src[0])+' FROM '+outtable+' AS t1 LEFT JOIN '+srctable+' AS t2 ON t1.'+str(linkf_out)+'=t2.'+str(linkf_src))
    sqexec(connection,'DROP TABLE '+str(outtable))
    copy_table(connection, 'tmp', outtable,withdata=True)
    sqexec(connection,'DROP TABLE tmp')

def create_geo_table(connection, name, gtype, srid, fields):
    #create a new table with geometry
    #'POINT','LINESTRING','POLYGON','MULTIPOINT','MULTILINESTRING','MULTIPOLYGON','GEOMETRYCOLLECTION'
    #EXAMPLE: create_geo_table(conn, 'b_buff_20', 'POLYGON', 25832, ['bid CHARACTER(36)','nobld INTEGER','barea REAL','tarea REAL'])
    statement="CREATE TABLE "+name+" (PK_UID INTEGER PRIMARY KEY AUTOINCREMENT"
    for val in fields:
        statement=statement+',' + val
    statement=statement+')'
    sqexec(connection, statement)
    statement="SELECT AddGeometryColumn('" + name + "','Geometry'," + str(srid) + ",'" + gtype + "',2)"
    sqexec(connection, statement)
    statement="SELECT CreateSpatialIndex('"+name+"','Geometry')"
    sqexec(connection, statement)

def create_table(connection, name, fields):
    #create a new table without geometry
    #EXAMPLE: create_table(conn, 'b_buff_20', ['bid CHARACTER(36)','nobld INTEGER','barea REAL','tarea REAL'])
    statement="CREATE TABLE "+name+" (PK_UID INTEGER PRIMARY KEY AUTOINCREMENT"
    for val in fields:
        statement=statement+',' + val
    statement=statement+')'
    sqexec(connection, statement)

def spatial_link_centroid(connection,nlinklayer,nbaselayer,field_linkid,field_baselayer=None):
    #find PK_UID's of features (parcels) that contain smaller features (buildings)
    #EXAMPLE: spatial_link_centroid(conn,'building_tmp','parcel_tmp','pid')
    #nlinklayer-contains features for which we try to find out which features in nbaselayer contain them (e.g.,buildings)
    #nbaselayer-base layer with larger features that contain those in nlinklayer (e.g., parcels)
    #field_linkid - id field which will be created in nlinklayer to save the connecting feature ID's (PK_UID)
    #field_baselayer-ID field from base layer that will be written into the linked features. MUST BE INTEGER. PK_UID will be used if None.
    if field_baselayer is None: field_baselayer='PK_UID'
    sqexec(connection,'ALTER TABLE '+nlinklayer+' ADD '+field_linkid+' INTEGER')
    sqexec(connection,'UPDATE '+nlinklayer+' SET '+field_linkid+'=(SELECT P.'+field_baselayer+' FROM '+nbaselayer+' as P WHERE ST_WITHIN( CENTROID('+nlinklayer+'.Geometry), P.Geometry) == 1 AND P.ROWID IN (SELECT ROWID FROM SpatialIndex WHERE f_table_name ="'+nbaselayer+'" AND search_frame = '+nlinklayer+'.Geometry ORDER BY ROWID))')

def spatial_link_intersect(connection,nlinklayer,nbaselayer,field_linkid,field_baselayer=None):
    #find PK_UID's of features (parcels) that contain smaller features (buildings)
    #EXAMPLE: spatial_link_intersect(conn,'building_tmp','parcel_tmp','pid')
    #nlinklayer-contains features for which we try to find out which features in nbaselayer contain them (e.g.,buildings)
    #nbaselayer-base layer with larger features that contain those in nlinklayer (e.g., parcels)
    #field_linkid - id field which will be created in nlinklayer to save the connecting feature ID's (PK_UID)
    #field_baselayer-ID field from base layer that will be written into the linked features. MUST BE INTEGER. PK_UID will be used if None.
    if field_baselayer is None: field_baselayer='PK_UID'
    sqexec(connection,'ALTER TABLE '+nlinklayer+' ADD '+field_linkid+' INTEGER')
    sqexec(connection,'UPDATE '+nlinklayer+' SET '+field_linkid+'=(SELECT P.'+field_baselayer+' FROM '+nbaselayer+' as P WHERE ST_INTERSECTS( '+nlinklayer+'.Geometry, P.Geometry) == 1 AND P.ROWID IN (SELECT ROWID FROM SpatialIndex WHERE f_table_name ="'+nbaselayer+'" AND search_frame = '+nlinklayer+'.Geometry ORDER BY ROWID) LIMIT 1)')
    
def spatial_link_area(connection,layer1,layer2,outtable,idfield1,idfield2):
    #compute intersection between layer1 and layer2
    #link features of layer2 to those of layer1 with which they share the biggest intersection area
    #the output is a 1:many connection!!! - each feature in layer1 can be connected to many features from layer 2, but not vice versa
    tscheme1=connection.execute("pragma table_info('"+layer1+"')").fetchall()
    id1_ind=[x[1] for x in tscheme1].index(idfield1)
    tscheme2=connection.execute("pragma table_info('"+layer2+"')").fetchall()
    id2_ind=[x[1] for x in tscheme2].index(idfield2)
    ##################################################
    geodef=connection.execute("SELECT srid,geometry_type FROM geometry_columns WHERE f_table_name='"+layer1.lower()+"'").fetchall()
    srid=geodef[0][0]
    create_geo_table(connection, 'isect_tmp', 'MULTIPOLYGON', srid, ['area DOUBLE PRECISION','f1 '+tscheme1[id1_ind][2],'f2 '+tscheme2[id2_ind][2]])
    sqexec(connection,'INSERT INTO isect_tmp(Geometry,f1,f2) SELECT SanitizeGeometry(CastToMultiPolygon(intersection(l1.Geometry,l2.Geometry))), l1.'+idfield1+',l2.'+idfield2+' FROM '+layer1+' AS l1,'+layer2+' AS l2 WHERE l2.ROWID IN (SELECT ROWID FROM SpatialIndex WHERE f_table_name="'+layer2+'" AND search_frame=l1.Geometry)')
     #TRY THE BELOW FOR SPEED
     #SELECT map1.*
     #FROM map1, map2
     #WHERE ST_Touches(map1.geometry, map2.geometry)
     #  AND map2.ROWID IN (
     #      SELECT pkid
     #        FROM idx_map1_geometry
     #       WHERE pkid MATCH RTreeIntersects(
     #             MbrMinX(map1.geometry),
     #             MbrMinY(map1.geometry),
     #             MbrMaxX(map1.geometry),
     #             MbrMaxY(map1.geometry)));
    
    sqexec(connection,'UPDATE isect_tmp SET area = round(Area(Geometry),1)')
    sqexec(connection,'DELETE FROM isect_tmp WHERE isect_tmp.area IS NULL or isect_tmp.area < 1')
    create_table(connection, 'isect_tmp2', ['f1 '+tscheme1[id1_ind][2],'f2 '+tscheme2[id2_ind][2]])
    sqexec(connection,'INSERT INTO isect_tmp2(f1,f2) SELECT t1.f1, t1.f2 FROM isect_tmp AS t1 INNER JOIN (SELECT f1 AS f11,f2 AS f22,max(area) AS amax FROM isect_tmp group by isect_tmp.f2) AS t2 ON t1.area=t2.amax and t1.f2=t2.f22')
    create_table(connection,outtable,[idfield1+' '+tscheme1[id1_ind][2],idfield2+' '+tscheme2[id2_ind][2]])
    sqexec(connection,'INSERT INTO '+outtable+'('+idfield1+','+idfield2+') SELECT t2.f1,t1.'+idfield2+' FROM '+layer2+' AS t1 LEFT JOIN (SELECT f1, f2 FROM isect_tmp2 GROUP BY f2) AS t2 ON t1.'+idfield2+'=t2.f2')
    sqexec(connection,"SELECT DropGeoTable('isect_tmp')")
    sqexec(connection,"SELECT DropGeoTable('isect_tmp2')")


    
def get_intersection_areas(connection,blayer,ilayer,groupfield,areafield):
    #compute intersection areas for each feature in blayer (=compute impervious areas for each subcatchment)
    #intersect blayer and ilayer, for each polygon in blayer save sum of intersection polygon areas from ilayer into areafield
    #EXAMPLE: get_intersection_areas(conn,'b_buff_20','buildings','bid','barea')
    tscheme=connection.execute("pragma table_info('"+blayer+"')").fetchall()
    groupind=[x[1] for x in tscheme].index(groupfield)
    geodef=connection.execute("SELECT srid,geometry_type FROM geometry_columns WHERE f_table_name='"+blayer.lower()+"'").fetchall()
    srid=geodef[0][0]
    create_geo_table(connection, 'isect_tmp', 'MULTIPOLYGON', srid, ['area DOUBLE PRECISION',groupfield+' '+tscheme[groupind][2]])
    sqexec(connection,"INSERT INTO isect_tmp (Geometry, "+groupfield+") SELECT CastToMultiPolygon(intersection("+blayer+".Geometry, "+ilayer+".Geometry)), "+blayer+"."+groupfield+" FROM "+blayer+","+ilayer+" WHERE "+ilayer+".ROWID IN (SELECT ROWID FROM SpatialIndex WHERE f_table_name='"+ilayer+"' AND search_frame="+blayer+".Geometry)")
    sqexec(connection,"UPDATE isect_tmp SET Geometry=SanitizeGeometry(Geometry)")
    sqexec(connection,"UPDATE isect_tmp SET area = Area(Geometry)")
    sqexec(connection,"DELETE FROM isect_tmp WHERE isect_tmp.area IS NULL or isect_tmp.area < 1")
    sqexec(connection,"CREATE TABLE isect_areas ("+groupfield+' '+tscheme[groupind][2]+", area_sum REAL)")
    sqexec(connection,"INSERT INTO isect_areas("+groupfield+", area_sum) SELECT "+groupfield+", sum(area) FROM isect_tmp GROUP BY "+groupfield)
    sqexec(connection,"SELECT DropGeoTable('isect_tmp')")
    #insert areas back into baselayer
    update_geotab_fields_ijoin(connection,blayer,'isect_areas',[areafield],['area_sum'],groupfield,groupfield)
    sqexec(connection,"DROP TABLE isect_areas")
   
def get_intersection_numbers(connection,blayer,ilayer,groupfield,iidfield,countfield):
    #compute number of features from ilayer that intersect each feature in blayer (=no. of buildings in each subcatchment)
    #intersect blayer and ilayer, for each polygon in blayer save number of intersecting polygons from ilayer into countfield
    #group intersection polygons by iidfield, to count each building only once
    #connection - db connection
    #blayer - layer with features over which intersection results will be summarized
    #ilayer - layer containing features that will be counted (how many features from ilayer intersect each feature of blayer)
    #iidfield - unique ID field on ilayer, used to filter potential splits of a building into multiple subpolygons within the same feature of blayer
    #countfield - field for saving counts in blayer
    #groupfield - unique ID field on blayer which is used for grouping intersection results
    #EXAMPLE: get_intersection_numbers(conn,'b_buff_20','bld_no_outhouse','bid','dyn_id','nobld')
    tscheme=connection.execute("pragma table_info('"+blayer+"')").fetchall()
    groupind=[x[1] for x in tscheme].index(groupfield)
    itscheme=connection.execute("pragma table_info('"+ilayer+"')").fetchall()
    igroupind=[x[1] for x in itscheme].index(iidfield)
    geodef=connection.execute("SELECT srid,geometry_type FROM geometry_columns WHERE f_table_name='"+blayer+"'").fetchall()
    srid=geodef[0][0]
    create_geo_table(connection, 'isect_tmp', 'MULTIPOLYGON', srid, ['area DOUBLE PRECISION',groupfield+' '+tscheme[groupind][2],iidfield+' '+itscheme[igroupind][2]])
    sqexec(connection,"INSERT INTO isect_tmp (Geometry,"+groupfield+","+iidfield+") SELECT CastToMultiPolygon(intersection("+blayer+".Geometry, "+ilayer+".Geometry)), "+blayer+"."+groupfield+","+ilayer+"."+iidfield+" FROM "+blayer+","+ilayer+" WHERE "+ilayer+".ROWID IN (SELECT ROWID FROM SpatialIndex WHERE f_table_name='"+ilayer+"' AND search_frame="+blayer+".Geometry)")
    sqexec(connection,"UPDATE isect_tmp SET Geometry=SanitizeGeometry(Geometry)")
    sqexec(connection,"UPDATE isect_tmp SET area = Area(Geometry)")
    sqexec(connection,"DELETE FROM isect_tmp WHERE isect_tmp.area IS NULL or isect_tmp.area < 5")
    sqexec(connection,"CREATE TABLE isect_areas ("+groupfield+' '+tscheme[groupind][2]+", icount INTEGER)")
    sqexec(connection,"CREATE TABLE isect_areas2 ("+groupfield+' '+tscheme[groupind][2]+", icount INTEGER)")
    #test this - how many results do we get (should be more records than buffered buildings, as there can be multiple buildings in the same buffer area). The aim here would be to only merge those records where the same intersection building was divided into multiple subpolygons.
    sqexec(connection,"INSERT INTO isect_areas("+groupfield+", icount) SELECT "+groupfield+", count(*) FROM isect_tmp GROUP BY "+groupfield+","+iidfield)
    sqexec(connection,"INSERT INTO isect_areas2("+groupfield+", icount) SELECT "+groupfield+", count(*) FROM isect_areas GROUP BY "+groupfield)
    sqexec(connection,"SELECT DropGeoTable('isect_tmp')")
    #insert areas back into baselayer
    update_geotab_fields_ijoin(connection,blayer,'isect_areas2',[countfield],['icount'],groupfield,groupfield)
    sqexec(connection,"DROP TABLE isect_areas")
    sqexec(connection,"DROP TABLE isect_areas2")

def update_geotab_fields_ijoin(connection,ilayer,jlayer,ifields,jfields,ilink,jlink):
    #update fields in a geotable with values from other table
    #update ifields in ilayer with values from jfields in jlayer based on innerjoin between fields ilink and jlink
    #EXAMPLE: update_geotab_fields_ijoin(conn,'buildings_new','b_buff_20',['b20_tarea','b20_barea','b20_bno'],['tarea','barea','nobld'],'dyn_id','bid')
    copy_geo_table(connection, ilayer, 'jjtmp',withdata=False,dropold=False)
    tscheme=connection.execute("pragma table_info('"+ilayer+"')").fetchall()
    statement="INSERT INTO jjtmp("
    for val in tscheme:
        if val[1]=='PK_UID' or val[1] in ifields:
            continue
        else:
            statement=statement+val[1]+','
    for val in ifields: 
        statement=statement+val+','
    statement=statement[0:(len(statement)-1)]+") SELECT "
    for val in tscheme:
        if val[1]=='PK_UID' or val[1] in ifields:
            continue
        else:
            statement=statement+'i.'+val[1]+','
    for val in jfields: 
        statement=statement+'j.'+val+','
    statement=statement[0:(len(statement)-1)]+" FROM "+ilayer+" AS i LEFT JOIN "+jlayer+" AS j ON i."+ilink+"=j."+jlink
    sqexec(connection,statement)
    sqexec(connection,"SELECT DropGeoTable('"+ilayer+"')")
    copy_geo_table(connection, 'jjtmp', ilayer,withdata=True,dropold=True)
    
def copy_database_clip(psource,ptarget,extent):
    #connect input DB and get table information
    conn_in=connect_spatial_db(psource)
    cursor=conn_in.cursor()
    geotables_in=cursor.execute('SELECT f_table_name,srid FROM geometry_columns').fetchall()
    alltables_in=cursor.execute('SELECT name FROM sqlite_master WHERE type ="table" AND name NOT LIKE "idx_%"').fetchall()
    cursor.close();del cursor
    conn_in.close();del conn_in
    #create output database, insert extent layer and get standard table names
    conn_out=create_new_db(ptarget,template=None,memory=False)
    sqexec(conn_out,"CREATE TABLE 'TMP_EXTENT' (PK_UID INTEGER PRIMARY KEY AUTOINCREMENT)")
    gtype=['POINT','LINESTRING','POLYGON','MULTIPOINT','MULTILINESTRING','MULTIPOLYGON','GEOMETRYCOLLECTION'][2]
    sqexec(conn_out,"SELECT AddGeometryColumn('TMP_EXTENT','Geometry',"+str(geotables_in[0][1])+",'"+gtype+"',2)")
    sqexec(conn_out,"SELECT CreateSpatialIndex('TMP_EXTENT','Geometry')")
    erectangle=str(extent[0][0])+' '+str(extent[0][1])+','+str(extent[0][0])+' '+str(extent[1][1])+','+str(extent[1][0])+' '+str(extent[1][1])+','+str(extent[1][0])+' '+str(extent[0][1])+','+str(extent[0][0])+' '+str(extent[0][1])
    erectangle='POLYGON (('+erectangle+'))'
    #erectangle='LINESTRING('+erectangle+')'
    sqexec(conn_out,"INSERT INTO TMP_EXTENT(Geometry) SELECT GeomFromText('"+erectangle+"',"+str(geotables_in[0][1])+")")
    #sqexec(conn_out,"INSERT INTO TMP_EXTENT(Geometry) SELECT MakePolygon(GeomFromText('"+erectangle+"',"+str(geotables_in[0][1])+"))")  
    cursor=conn_out.cursor()
    standardtables_out=cursor.execute("SELECT name FROM sqlite_master WHERE type ='table' AND name NOT LIKE 'idx_%'").fetchall()
    cursor.close();del cursor
    #filter table names to copy
    geotables_in=[x[0] for x in geotables_in]
    standardtables_out=[x[0] for x in standardtables_out]
    alltables_in=[x[0] for x in alltables_in]
    alltables_in=[x for x in alltables_in if not x in standardtables_out]
    alltables_in=[x for x in alltables_in if not x in geotables_in]
    sqexec(conn_out,"ATTACH DATABASE '"+ psource + "'AS sdb")
    for tabname in alltables_in:
        print(tabname)
        copy_table(conn_out,tabname,tabname,sourcedbalias='sdb',withdata=True,dropold=False)
    for tabname in geotables_in:
        print(tabname)
        copy_geo_table(conn_out, tabname, tabname,sourcedbalias='sdb',withdata=True,dropold=False,exclude=[],cliplayer='TMP_EXTENT')
    conn_out.close();del conn_out




